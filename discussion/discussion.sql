-- SQL syntaxes are not case sensitive.
-- If you are going to add a SQL syntax (capitalize).
-- If you are going to use or add name of the column or table (small letter).

	-- To show all the list of our databases:
SHOW DATABASES;

	-- To create or add a database:
-- 1st way
CREATE DATABASE music_db;

-- 2nd way (manually)
-- open phpmyadmin and click NEW then write DB name then click create

	-- To drop/delete a database:
-- 1st way
DROP DATABASE music_db;

-- 2nd way (manually)
-- open phpmyadmin and click HOME/LOGO + click DATABASES + check the box of db you want to delete + click DROP then DROP again

	-- Select a database.
-- 1st way
USE music_db;

-- 2nd way (manually)
-- open phpmyadmin then click the file you want to use

	-- Show tabel inside a database:
SHOW TABLES;

	-- Creating a table;
-- CREATE TABLE table name - should be plural
-- id, username, password, etc - name of columns
-- VARCHAR means string
-- (50) means can only hold 50 characters
-- NOT NULL means cant be empty/required fields
-- PRIMARY KEY (property) - not automatic. should always be included
-- AUTO_INCREMENT - auto +1 every count
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

-- Mini-activity: You are going to create the artists table:
	-- name (Not nullable);
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

	-- To change the name of the Table:
-- 1st way
ALTER TABLE oldname RENAME newname;

-- 2nd way
RENAME TABLE oldname TO newname;

 
CREATE TABLE albums (
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL, 
	PRIMARY KEY(id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Mini-activity 2:
CREATE TABLE songs (
	id INT NOT NULL AUTO_INCREMENT,
	song_title VARCHAR(50) NOT NULL,
	song_length TIME NOT NULL,
	song_genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY(album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_playlists_users_id
		FOREIGN KEY(user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

	-- Multiple Foreign Key in one table.
CREATE TABLE playlists_songs (
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlist_songs_playlist_id
		FOREIGN KEY(playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlist_songs_song_id
		FOREIGN KEY(song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);